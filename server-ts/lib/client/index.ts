import { getString } from "../common/common.ts"

function get_rest_string() {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", "rest_interface", false)
  xmlHttp.send(null);
  return "REST interface said " + xmlHttp.responseText;
}

function get_direct_string() {
  return "Client code said " + getString();
}

export function update_string() {
  document.body.innerHTML = document.body.innerHTML = get_rest_string() + '<p>' + get_direct_string();
}