export function get_entry_points(): Array<String> {
  const entry_points = [
    'index.ts'
  ];
  return entry_points.map(entry => import.meta.dirname + "/" + entry);
}