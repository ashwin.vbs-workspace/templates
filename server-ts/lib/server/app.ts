import { Router } from "jsr:@oak/oak/router";

import { getString } from "../common/common.ts"

export class Application {
  constructor() { }
  getRouter() {
    let router = new Router();
    router.get("/", (context) => {
      context.response.body = getString();
    });
    return router;
  }
}