import { parse } from "https://deno.land/std@0.182.0/flags/mod.ts";
import { Application as OakApplication } from "jsr:@oak/oak/application";
import { Router as OakRouter } from "jsr:@oak/oak/router";

// Utils
import { build_files } from "./util/build.ts";

// client code
import { get_entry_points } from "./lib/client/manifest.ts";

// server code
import { Application } from "./lib/server/app.ts";

// Server application start
const static_serve_path = import.meta.dirname + "/www";
const args = parse(Deno.args, {
  boolean: ["debug"],
  string: ["cert", "key", "port", "host"],
  default: {
    host: "0.0.0.0",
    port: "8080",
  },
});

// Build (and start listen for changes if debug);
await build_files(get_entry_points(), static_serve_path, args.debug);


var application = new Application();


const oak_app = new OakApplication();
const rest_interface = new OakRouter().use("/rest_interface", application.getRouter().routes());
oak_app.use(rest_interface.routes());
oak_app.use(async (context, next) => {
  try {
    await context.send({
      root: static_serve_path,
      index: "index.html",
    });
  } catch {
    await next();
  }
});

const useTls = !!(args.key && args.cert);
await oak_app.listen(useTls ? {
  port: args.port,
  hostname: args.host,
  certFile: args.cert,
  keyFile: args.key,
} : {
  port: args.port,
  hostname: args.host,
});
