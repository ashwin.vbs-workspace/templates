import * as esbuild from 'https://deno.land/x/esbuild@v0.20.2/mod.js'
import httpFetch from 'https://deno.land/x/esbuild_plugin_http_fetch@v1.0.3/index.js'


export async function build_files(input_paths: Array<String>, outdir_path: String, is_debug: boolean): Promise<void> {
  const build_configuration = {
    entryPoints: input_paths,
    bundle: true,
    outdir: outdir_path,
    plugins: [httpFetch],
    globalName: "test",
  };

  const build_output = await esbuild.build(build_configuration);
  if (build_output.errors.length > 0 || build_output.warnings.length > 0)
    console.log();

  if (is_debug) {
    const ctx = await esbuild.context(build_configuration);
    await ctx.watch();

    console.log("Watching source files for changes");
  }
}