{
  description = "cpp template project";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }:
  let pkgs = import nixpkgs { system = "x86_64-linux"; };
  in {
    devShells.x86_64-linux.default = pkgs.mkShell {
      packages = with pkgs; [
        clang
        meson
        ninja
        # gtest
        # pkg-config
      ];
      shellHook = ''
        export PS1="[\[\e[01;32m\]nix-shell\[\e[m\] \[\e[01;34m\]\w\[\e[m\]]$ "
        export CC=clang
        export CXX=clang++
      '';
    };
  };
}
